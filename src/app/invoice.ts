export class Invoice {
  id: number;
  paymentDueDate: string;
  clientName: string;
  clientEmail: string;
  jobDesc: string;
  total: number;
}