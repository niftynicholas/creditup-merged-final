import { Component, OnInit} from '@angular/core';
import { RetrieveService } from '../shared/retrieve.service';
import { DataService } from '../shared/data.service';
import { Invoice } from '../invoice';
import { Router } from '@angular/router';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {
  invoices: any;
  selectedInvoice: Invoice;
  paymentDueDate: string;
  clientName: string;
  clientEmail: string;
  jobDesc: string;
  total: number;

  doc = new jsPDF({
    orientation: 'portrait'
  })

  constructor(private dataService: DataService, private router: Router) { 
  }

  ngOnInit() {
    this.dataService.getInvoices()
      .subscribe(invoices => this.invoices = invoices);
    console.log("ngOnInit");
    console.log(this.invoices);
    console.log("---");
    this.invoices.unsubscribe;
  }

  onSelect(invoice: Invoice): void {
    this.selectedInvoice = invoice;
    localStorage.setItem('selectedInvoice', this.selectedInvoice.id + '');
    this.router.navigateByUrl('/viewinvoice');
  }

  save() {
    this.doc.text(35, 25, "TEST");
    this.doc.save('report.pdf');
  }
}