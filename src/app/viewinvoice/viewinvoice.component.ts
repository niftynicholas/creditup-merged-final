import { Component, OnInit } from '@angular/core';
import { Invoice } from '../invoice';
import { DataService } from '../shared/data.service';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-viewinvoice',
  templateUrl: './viewinvoice.component.html',
  styleUrls: ['./viewinvoice.component.css']
})
export class ViewinvoiceComponent implements OnInit {
  invoices: Invoice[];
  selectedInvoice: Invoice;
  id: number;  

  constructor(private dataService: DataService) { 
  }

  ngOnInit() {
    this.dataService.getInvoices()
      .subscribe(invoices => this.invoices = invoices);
  	this.id = Number(localStorage.getItem('selectedInvoice')); // convert to number
  	this.selectedInvoice = this.invoices[this.id-1];
  }
}
