import { Component, OnInit} from '@angular/core';
import { RetrieveService } from '../shared/retrieve.service';
import { DataService } from '../shared/data.service';
import * as Highcharts from "highcharts";


@Component({
  selector: 'app-financialplanning',
  templateUrl: './financialplanning.component.html',
  styleUrls: ['./financialplanning.component.css']
})
export class FinancialPlanningComponent implements OnInit {
  
 datalist:any = [];
 imageroute = 'assets/images/';
 CardNumber: any;

   onSetCard(i) {
    let dataService = new DataService();
    dataService.CardNumber.next(i); 
    this.CardNumber = i;
    this.datalist.unsubscribe;

  }

  constructor() { 
    
  }

  ngOnInit() {
    this.incomeAnalysisChart();
    this.expenditureDecompChart();
    this.monthlyAverageChart();
    this.goalsLineChart();
  }
    incomeAnalysisChart(){
      let myChart = Highcharts.chart("timeSeriesIncomeAnalysis",{
        chart: {
          type: 'areaspline'
        },
        title: {
          text: null
        },
        legend: {
          layout: 'vertical',
          align: 'left',
          verticalAlign: 'top',
          x: 150,
          y: 100,
          floating: true,
          borderWidth: 1,
        //  backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
          categories: [
            'January',
            'February',
            'March',
            'April',
            'June',               
            'July',
            'August'
          ],
          plotBands: [{ // visualize the weekend
            from: 4.5,
            to: 6.5,
            color: 'rgba(68, 170, 213, .2)'
          }]
        },
        yAxis: {
          title: {
            text: 'Time'
          }
        },
        tooltip: {
          shared: true,
          valueSuffix: ' units'
        },
        credits: {
          enabled: false
        },
        plotOptions: {
          areaspline: {
            fillOpacity: 0.5
          }
        },
        series: //seriesData
        [{
          name: 'Professional Income',
          data: [3, 4, 3, 5, 4, 10, 12]
        }, {
          name: 'Net Worth',
          data: [1, 3, 4, 3, 3, 5, 4]
        },{
          name: 'Expenditure',
          data: [3, 3, 2, 3, 5, 1, 4]
        }]  
      });
      
    };

    expenditureDecompChart(){
      // Build the chart
      let myExpenditureChart = Highcharts.chart('expPieChart', {  
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Your Current Monthly Expenditure Decomposition'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Category',
            colorByPoint: true,
            data: [{
                name: 'Transportation',
                y: 61.41,
                sliced: true,
                selected: true
            }, {
                name: 'Dining',
                y: 11.84
            }, {
                name: 'Utilities',
                y: 10.85
            }, {
                name: 'Entertainment',
                y: 4.67
            }, {
                name: 'Shopping',
                y: 4.18
            }, {
                name: 'Other',
                y: 7.05
            }]
            }]
          });
        }
    monthlyAverageChart(){

      let myAverageChart = Highcharts.chart('catMonthAveChart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Expenditure by Category'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Transportation',
                'Dining',
                'Utilities',
                'Entertainment',
                'Shopping',
                'Other'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Amount ($)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Average Monthly Expenditure',
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0]
    
        }, {
            name: 'Current Monthly Expenditure',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5]
    
        }]
    });
    }

    goalsLineChart(){
      let myGoalsChart = Highcharts.chart('compareToGoalChart', {

        title: {
            text: 'My Goal Tracking Chart'
        },
        yAxis: {
            title: {
                text: 'Amount ($)'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },
    
        series: [{
            name: 'Balance',
            data: [43934, 52503, 57177, 60658, 65000, 73000, 76000, 83000]
        }, {
            name: 'Goal',
            data: [100000, 100000, 100000, 100000,100000,100000,100000,100000]
        }],
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
    }
  }
    