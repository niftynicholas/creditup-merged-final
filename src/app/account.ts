export class Account {
  bank: string;
  accountName: string;
  accountDescription: string;
}