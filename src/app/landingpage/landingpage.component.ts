import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.css']
})

export class LandingPageComponent implements OnInit {

  title = 'Credit Cards';
  banner = 'assets/images/Citi.png';
  imageroute = 'assets/images/'; 

  constructor() { }

  ngOnInit() { }
}
