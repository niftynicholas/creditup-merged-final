import { Component, OnInit} from '@angular/core';
import { RetrieveService } from '../shared/retrieve.service';
import { DataService } from '../shared/data.service';
import { Globals } from '../globals';
import { Account } from '../account';
import { ViewChild,ElementRef } from '@angular/core'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent implements OnInit {
  accounts: Account[];
  accountDescription: string;
  accountName: string;
  jobDesc: string;
  total: number;
  selectedBank: string;
  @ViewChild('selectedBankTag') selectedBankTag: ElementRef;

  CitibankImage = 'assets/images/Citi.png';
  DBSImage = 'assets/images/Citi.png';
  OCBCImage = 'assets/images/Citi.png';
  UOBImage = 'assets/images/Citi.png';

  constructor(private router: Router) { 
  }

  ngOnInit() {
  }

  submit() {
    localStorage.setItem("bankAcctAdded", "true");
    console.log(this.selectedBankTag.nativeElement.value);
    console.log(this.accountName);
    console.log(this.accountDescription);
    this.router.navigateByUrl('/account');
    // this.invoices.push({id: this.invoices.length + 1, paymentDueDate: this.paymentDueDate, clientName: this.clientName, jobDesc: this.jobDesc, total: this.total});
    // this.globals.invoices = this.invoices;
    // console.log(this.globals.invoices);
    // this.paymentDueDate = '';
    // this.clientName = '';
    // this.jobDesc = '';
    // this.total = 0.00;
  }


}