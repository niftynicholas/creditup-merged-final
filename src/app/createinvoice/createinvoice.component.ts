import { Component, OnInit} from '@angular/core';
import { RetrieveService } from '../shared/retrieve.service';
import { DataService } from '../shared/data.service';
import { Invoice } from '../invoice';
import { Router } from '@angular/router';
@Component({
  selector: 'app-invoice',
  templateUrl: './createinvoice.component.html',
  styleUrls: ['./createinvoice.component.css']
})
export class CreateInvoiceComponent implements OnInit {
  invoices: Invoice[];
  selectedInvoice: Invoice;
  paymentDueDate: string;
  clientName: string;
  clientEmail: string;  
  jobDesc: string;
  total: number;

  constructor(private dataService: DataService, private router: Router) { 
  }

  ngOnInit() {
    this.dataService.getInvoices()
      .subscribe(invoices => this.invoices = invoices);
    console.log("ngOnInit");
    console.log(this.invoices);
    console.log("---");
  }

  onClick() {
    this.invoices.push({id: this.invoices.length + 1, paymentDueDate: this.paymentDueDate, clientName: this.clientName, clientEmail: this.clientEmail, jobDesc: this.jobDesc, total: this.total});
    this.dataService.setInvoices(this.invoices);
    this.paymentDueDate = '';
    this.clientName = '';
    this.clientEmail = '';
    this.jobDesc = '';
    this.total = 0.00;
    console.log(this.dataService.invoices);
    this.router.navigateByUrl('/invoice');
  }
}